variable "regions" {
  type = map(string)
  default = {
    "0" = "us-central1"
    "1" = "us-east1"
    "2" = "us-west1"
  }
}

variable "runners" {
  type = map(string)
  default = {
    "0" = "runner"
    "1" = "stage"
    "2" = "prod"
  }
}
variable "studentname" {
  type        = string
  description = "The student name"
}

variable "studentsurname" {
  type        = string
  description = "The student surename"
}

variable "project" {
  type    = string
  default = "saroka-gc-bootcamp"
}

# variable "subnets" {
#   type = list(string)
# }

variable "nodes" {
  type = number
}

variable "runnerkey" {
  type        = string
  description = "Gitlab runner key"
}

